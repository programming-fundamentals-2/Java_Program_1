    package wSpeed;
    
    /**
     * WindSpeed is responsible for converting different
     * measurments of wind velocity like km/h, knots and
     * beaufort.
     * 
     * @author Christian Pape
     * @author Liam Hockley (edit 2/10/2012) REASON: Simplification
     */
    
    
    public class WindSpeed {    
        /**
         * the wind speed in kilometer per hour
         */
        private double kilometerPerHour = 0.0;
        
    
         /**
          * Creates a new wind speed with the speed
          * of the given kilometer per hour.
          */
        public WindSpeed(double AssignedDefVal) {
        	//constructor with an assigned value for the wind
            kilometerPerHour = AssignedDefVal;
            
        }
        
        public WindSpeed() {
        	//Constructor with no parameters. (assumes no wind)
            kilometerPerHour = 0.00;
            
        }
        
        /**
         * Return true if this wind speed is a 
         * European windstorm (orcan, more than
         * 120 km/h).
         */
        public boolean IsOrcan() {
            return kilometerPerHour > 120.0;
        }
    
        /**
         * Return true if this wind speed is 
         * calm (less than 2 km/h).
         */
        public boolean isCalm() {
            return kilometerPerHour < 2.0;
        }
       
        
        /**
         * Returns the wind speed measured in knots (nautic miles).
         * A nautic mile is 1.852 kilometer.
         */
        public double getKnots() {
            return (kilometerPerHour / 1.852);
        }
        
       
        /**
         * Returns the wind speed measured in Beaufort (1-12).
         */       
        public int getBeaufort() {
            int beaufort =  (int) (Math.pow(kilometerPerHour / 3.01, 0.6666) +0.5);
            
            if (beaufort > 12) {
                beaufort = 12;
            }
            
            return beaufort;
        }
 }



























































