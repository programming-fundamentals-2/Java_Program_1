package wSpeed;

import java.util.Scanner;
import java.text.*;

public class Jmain {

	/**
	 * @param args
	 */
	public static void main(String[] args)
	//declare expectd exception status for main() because of Thread.sleep()
	throws InterruptedException{
			//init vars
			double A=0.00, B=0.00;
			//init scanner
			Scanner in = new Scanner (System.in);
			//init  decimal format
			DecimalFormat df = new DecimalFormat("#.##"); //to make the Knots value more readable
        
			//Please enter the wind speed A (Km/h)
			System.out.print("Please enter the wind speed A (Km/h): ");
			A=in.nextDouble();
			//Please enter the wind speed B (Km/h)
			System.out.print("Please enter the wind speed B (Km/h): ");
			B=in.nextDouble();

			//BEGIN THE MAGIC! (create the objects)
			WindSpeed WindyA = new WindSpeed(A);
			WindSpeed WindyB = new WindSpeed(B);
			
			System.out.println();//spacer
			java.awt.Toolkit.getDefaultToolkit().beep();//beep
			System.out.println("Thinking...");//spacer
			System.out.println();//spacer
			Thread.sleep(3000L);//pause
		
			//The current wind speed in Beaufort for A is:
			System.out.println("The current wind speed in Beaufort for A is: " + WindyA.getBeaufort());
			Thread.sleep(500L);//pause
			//This is {x} more than A (A-B)
			System.out.print("This is " + (WindyB.getBeaufort()-WindyA.getBeaufort()) +" more than B... ");
			Thread.sleep(500L);//pause
			//Therefore the current wind speed in Beaufort for B is:
			System.out.println("Therefore the current wind speed in Beaufort for B is: " + WindyB.getBeaufort());
		
			java.awt.Toolkit.getDefaultToolkit().beep();//beep
			Thread.sleep(1000L);//pause
			System.out.println();//spacer
			
			//The current wind speed in Knots for A is:
			System.out.println("The current wind speed in Knots for A is: " + df.format(WindyA.getKnots()));
			Thread.sleep(500L);//pause
			//This is {x} more than A (A-B)
			System.out.print("This is " + df.format((WindyB.getKnots()-WindyA.getKnots())) + " more than B... ");
			Thread.sleep(500L);//pause
			//Therefore the current wind speed in Beaufort for B is:
			System.out.println(" Therefore the current wind speed in Knots for B is: " + df.format(WindyB.getKnots()));
			System.out.println();//spacer
			
			java.awt.Toolkit.getDefaultToolkit().beep();//beep
			Thread.sleep(1000L);//pause
			java.awt.Toolkit.getDefaultToolkit().beep();//beep
			System.out.println("Thank you for using this wonderful program!");	
	}
}
